const process = require('node:process');

const production = process.env.NODE_ENV === 'production';

/** @type {import('tailwindcss').Config} */
module.exports = {
	plugins: {
		tailwindcss: {},
		autoprefixer: {},
		...(production ? {cssnano: {}} : {}),
	},
};
