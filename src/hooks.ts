import type {Handle} from '@sveltejs/kit';

export const handle: Handle = async ({event, resolve}) => {
	let lang = 'ru';
	const pathname = event.url.pathname;
	if (pathname.includes('/en/') || pathname.endsWith('/en')) {
		lang = 'en';
	} else if (pathname.includes('/zh/') || pathname.endsWith('/zh')) {
		lang = 'zh';
	} else if (pathname.includes('/ko/') || pathname.endsWith('/ko')) {
		lang = 'ko';
	}

	const response = await resolve(event, {
		transformPageChunk: ({html}) => html.replace('<html>', `<html lang="${lang}">`),
	});
	return response;
};
