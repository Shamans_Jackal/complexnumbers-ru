import subsrt from 'subsrt';
import type {Plugin} from 'unified';
import {u} from 'unist-builder';

const parse: Plugin = function (options?: subsrt.ParseOptions) {
	const parserFn = (doc: string) => {
		const nodes = subsrt.parse(doc, options);

		const tree = u('root', nodes);
		return tree;
	};

	this.Parser = parserFn;
};

export default parse;
