import type {Plugin} from 'unified';
import type {Root, Text} from 'mdast';
import {visit} from 'unist-util-visit';

const rehypeEllipsis: Plugin = function () {
	return (tree: Root) => {
		visit(tree, 'text', (node: Text) => {
			node.value = node.value.replace(/\.{3}/gim, '…');
		});
	};
};

export default rehypeEllipsis;
