import type {Parent as UnistParent, Literal as Node} from 'unist';

export type Parent = {
	children: Content[];
} & UnistParent;

export type Content =
    | MetaContent
    | StyleContent
    | CaptionContent;

export type MetaContent = Meta;

export type StyleContent = Style;

export type CaptionContent = Caption;

export type Root = {
	type: 'root';
} & Parent;

export type Meta = {
	type: 'meta';
} & Node;

export type Style = {
	type: 'style';
} & Node;

export type Caption = {
	type: 'caption';
	start: number;
	end: number;
	duration: number;
	content: string;
	text: string;
} & Node;
