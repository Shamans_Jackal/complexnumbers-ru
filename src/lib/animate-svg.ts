
type Options = {
	delay?: number;
	duration?: number;
	strokeWidth?: number;
	timingFunction?: string;
	strokeColor?: string;
	fillColor?: string;
	repeat?: boolean;
};

const keyframesTemplate = `
@-webkit-keyframes svg-text-anim {
    40% {
        stroke-dashoffset: 0;
        fill: transparent;
    }
    60% {
        stroke-dashoffset: 0;
        fill: %FILL%;
    }
    100% {
        stroke-dashoffset: 0;
        fill: %FILL%;
    }
}

@keyframes svg-text-anim {
    40% {
        stroke-dashoffset: 0;
        fill: transparent;
    }
    60% {
        stroke-dashoffset: 0;
        fill: %FILL%;
    }
    100% {
        stroke-dashoffset: 0;
        fill: %FILL%;
    }
}
`;

const animateSvg = (element: HTMLElement, options: Options) => {
	const delay = options.delay ?? 0;
	const duration = options.duration ?? 1;
	const strokeWidth = options.strokeWidth ?? 1;
	const timingFunction = options.timingFunction ?? 'linear';
	const strokeColor = options.strokeColor ?? '#ffffff';
	const fillColor = options.fillColor ?? '#ffffff';
	const repeat = options.repeat ?? false;

	const keyframes = keyframesTemplate.replace(/%FILL%/g, fillColor);

	const svgs = element.querySelectorAll('svg');
	for (const svg of Array.from(svgs)) {
		const xmlns = 'http://www.w3.org/2000/svg';
		const defs = document.createElementNS(xmlns, 'defs');
		const style = document.createElementNS(xmlns, 'style');
		style.setAttribute('type', 'text/css');
		const node = document.createTextNode(keyframes);
		style.append(node);
		defs.append(style);
		svg.append(defs);
	}

	const paths = element.querySelectorAll('path');
	const mode = repeat ? 'infinite' : 'forwards';
	for (const [i, path] of Array.from(paths).entries()) {
		const length = path.getTotalLength();
		path.style['stroke-dashoffset'] = `${length}px`;
		path.style['stroke-dasharray'] = `${length}px`;
		path.style['stroke-width'] = `${strokeWidth}px`;
		path.style.stroke = `${strokeColor}`;
		path.style.animation = `${duration}s svg-text-anim ${mode} ${timingFunction}`;
		path.style['animation-delay'] = `${i * delay}s`;
	}
};

export default animateSvg;
