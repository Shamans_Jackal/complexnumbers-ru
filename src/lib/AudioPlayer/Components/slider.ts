export default function handle(node: HTMLElement) {
	const onDown = getOnDown(node);
	const onMouseOver = getOnMouseOver(node);

	node.addEventListener('touchstart', onDown);
	node.addEventListener('mousedown', onDown);
	node.addEventListener('mouseover', onMouseOver);
	return {
		destroy() {
			node.removeEventListener('touchstart', onDown);
			node.removeEventListener('mousedown', onDown);
			node.removeEventListener('mouseover', onMouseOver);
		},
	};
}

function getOnMouseOver(node: HTMLElement) {
	function onMouseMove(event: MouseEvent) {
		dispatchDragEvent('dragover', node, event);
	}

	return (event: MouseEvent) => {
		const onMouseOut = (event: MouseEvent | TouchEvent) => {
			event.stopPropagation();
			document.removeEventListener('mousemove', onMouseMove);
			document.removeEventListener('mouseout', onMouseOut);
		};

		if (event.cancelable) {
			event.preventDefault();
		}

		document.addEventListener('mousemove', onMouseMove);
		document.addEventListener('mouseout', onMouseOut);
	};
}

function getOnDown(node: HTMLElement) {
	function onMove(event: MouseEvent | TouchEvent) {
		dispatchDragEvent('drag', node, event);
	}

	return (event: MouseEvent | TouchEvent) => {
		const onUp = (event: MouseEvent | TouchEvent) => {
			event.stopPropagation();

			document.removeEventListener(moveevent, onMove);
			document.removeEventListener(upevent, onUp);

			dispatchDragEvent('dragend', node, event);
		};

		if (event.cancelable) {
			event.preventDefault();
		}

		dispatchDragEvent('dragstart', node, event);

		const moveevent = 'touches' in event ? 'touchmove' : 'mousemove';
		const upevent = 'touches' in event ? 'touchend' : 'mouseup';

		document.addEventListener(moveevent, onMove);
		document.addEventListener(upevent, onUp);
	};
}

function dispatchDragEvent(eventName: string, node: HTMLElement, event: MouseEvent | TouchEvent) {
	const {left, width} = node.getBoundingClientRect();

	const clickOffset = 'touches' in event
		? (event.touches.length > 0 ? event.touches[0].clientX : event.changedTouches[0].clientX)
		: event.clientX;
	const clickPos = Math.min(Math.max((clickOffset - left) / width, 0), 1) || 0;
	node.dispatchEvent(new CustomEvent(eventName, {detail: clickPos}));
}
