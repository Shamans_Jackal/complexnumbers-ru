export type Language = {
	code: string;
	name: string;
};

export type AudioFormat = {
	extension: string;
	mediaType: string;
	cutoff: number;
};

// Track fields from index.yaml file
export type BaseTrackMetadata = {
	page: string;
	instrumental?: boolean;
	subtitles?: string;
	duration: number;
	audioBasename: string;
	spotify?: string;
	amazon?: string;
	youtube?: string;
	appleMusic?: string;
	yandexMusic?: string;
	bilibili?: string;
	flac?: string;
};

// Format of index.yaml file
export type AlbumIndexYaml = {
	languages: Language[];
	tracks: BaseTrackMetadata[];
	themeColor: string;
	spashscreensPath: string;
	mirrors: string[];

	icon: string;
	appleIcon: string;
	manifestIcons: Array<{
		src: string;
		type: string;
		sizes: string;
	}>;

	formats: AudioFormat[];
};

/* Type of header for index.md files inside albums */
export type IndexPageMetadata = {
	title: string;
	artist: string;
	playlistTitle: string;
	image: string;
	overlay: string;
	formatters: {
		albumPageTitle: string;
		trackPageTitle: string;
		trackTitle: string;
		infoTitle: string;
	};
	meta: {
		description: string;
	};
};

/* Types of outgoing objects */
export type TrackMetadata = BaseTrackMetadata & {
	title: string;
	subtitle?: string;
	url: string;
};

export type AlbumMetadata = IndexPageMetadata & {
	about: string;
	languages: Language[];
	tracks: TrackMetadata[];
	themeColor: string;
	mirrors: string[];
	formats: AudioFormat[];

	spashscreensPath: string;
	icon: string;
	appleIcon: string;
	manifestIcons: Array<{
		src: string;
		type: string;
		sizes: string;
	}>;

	language: string;
	baseUrl: string;
	langUrl: string;
	webmanifest: string;
};

export type ArtistList = Array<{
	name: string;
	role: string;
	image?: string;
}>;

export type TrackPageMetadata = {
	title: string;
	subtitle?: string;
	image?: string;
	byArtist: ArtistList;
	translator?: string;
	translation?: string;
};

export type IndexPage = {
	metadata: IndexPageMetadata;
	content: string;
};

export type TrackPage = {
	metadata: TrackPageMetadata;
	content: string;
	timings?: Array<[number, number]>;
};
