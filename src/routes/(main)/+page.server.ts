import type {PageServerLoad} from './$types';
import {process} from '$lib/markdown';

export const load: PageServerLoad = () =>
	process('content/index.md');
