const translations = {
	en: {
		skipToContent: 'Skip to content',
		music: 'Music and video',
		history: 'History',
		articles: 'Articles',
		externalWebsites: 'Other websites',
		vk: 'VK',
		allMusicInFlac: 'All music in flac',
	},
	ru: {
		skipToContent: 'Skip to content',
		music: 'Музыка и видео',
		history: 'История',
		articles: 'Статьи',
		externalWebsites: 'Внешние сайты',
		vk: 'VK',
		allMusicInFlac: 'Вся музыка во flac',
	},
};

export default translations;
