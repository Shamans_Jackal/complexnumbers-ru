import type {PageServerLoad} from './$types';
import {processIndex} from './process';

export const load: PageServerLoad = ({params: {album, language}}) =>
	processIndex(album, language);
