import type {LayoutServerLoad} from './$types';
import type {AlbumMetadata, TrackMetadata} from '$lib/AudioPlayer/types';

import {loadIndexFile, loadIndexMd, loadTrackMdMetadata} from '$lib/AudioPlayer/utils';

export const load: LayoutServerLoad = ({params}) => {
	const {album, language} = params as {album: string; language: string};

	const albumIndex = loadIndexFile(album);
	const index = loadIndexMd(album, language);

	const tracks: TrackMetadata[] = albumIndex.tracks.map(trackMetadata => {
		const track = loadTrackMdMetadata(album, language, trackMetadata.page);
		const trackInfo = {
			title: track.title,
			subtitle: track.subtitle,
			...trackMetadata,
			url: `/listen/${album}/${language}/${trackMetadata.page}/`,
		};
		if (!trackMetadata.instrumental) {
			trackInfo.subtitles = `/listen/${album}/${language}/${trackMetadata.page}.ass`;
		}

		return trackInfo;
	});

	const body: AlbumMetadata = {
		...index.metadata,
		about: index.content,
		languages: albumIndex.languages,
		tracks,

		themeColor: albumIndex.themeColor,
		mirrors: albumIndex.mirrors,
		formats: albumIndex.formats,

		spashscreensPath: albumIndex.spashscreensPath,
		icon: albumIndex.icon,
		appleIcon: albumIndex.appleIcon,
		manifestIcons: albumIndex.manifestIcons,

		language,
		baseUrl: `/listen/${album}/`,
		langUrl: `/listen/${album}/${language}/`,
		webmanifest: `/listen/${album}/${language}/manifest.webmanifest`,
	};

	return body;
};
