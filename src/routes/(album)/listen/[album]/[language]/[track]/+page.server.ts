
import {processTrack} from '../process';
import type {PageServerLoad} from './$types';

export const load: PageServerLoad = ({params: {album, language, track}, url}) => {
	const page = processTrack(album, language, track);
	return {
		...page,
		key: url.pathname,
	};
};
