{
    "$schema": "http://json-schema.org/draft-07/schema",
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire document.",
    "required": [
        "languages",
        "themeColor",
        "spashscreensPath",
        "formats",
        "mirrors",
        "tracks"
    ],
    "properties": {
        "languages": {
            "type": "array",
            "minItems": 1,
            "title": "The languages schema",
            "description": "List of supported languages",
            "additionalItems": false,
            "items": {
                "type": "object",
                "title": "The items schema",
                "required": [
                    "code",
                    "name"
                ],
                "properties": {
                    "code": {
                        "type": "string",
                        "title": "Language code",
                        "description": "Short code of language, e. g. 'ru', should correspond the folder name with Markdown files",
                        "pattern": "^[\\w\\d-]+$"
                    },
                    "name": {
                        "type": "string",
                        "title": "Language name"
                    }
                },
                "additionalProperties": false
            }
        },
        "themeColor": {
            "type": "string",
            "title": "The themeColor schema for webmanifest"
        },
        "spashscreensPath": {
            "type": "string",
            "title": "Server path to directory with splashscreens",
            "description": "Can be generated with https://www.simicart.com/manifest-generator.html/"
        },
        "formats": {
            "type": "array",
            "minItems": 1,
            "title": "The list of published file types (e. g. mp3, opus, m4a)",
            "additionalItems": false,
            "items": {
                "type": "object",
                "title": "Description of media type",
                "required": [
                    "extension",
                    "mediaType",
                    "cutoff"
                ],
                "properties": {
                    "extension": {
                        "type": "string",
                        "title": "Extension of audio file",
                        "pattern": "^[\\w\\d]+$",
                        "examples": ["opus"]
                    },
                    "mediaType": {
                        "type": "string",
                        "title": "MediaType of file, will be passed to https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement/canPlayType"
                    },
                    "cutoff": {
                        "type": "number",
                        "title": "Bandwidth cutoff for stored audio files, used in visualiser"
                    }
                },
                "additionalProperties": false
            }
        },
        "mirrors": {
            "type": "array",
            "minItems": 1,
            "title": "The list of mirrors, providing audio files",
            "additionalItems": false,
            "items": {
                "type": "string",
                "title": "URL of mirror. Mirror is considered to be alive if it responses with {\"status\": \"ok\"} for <mirror url>/health.json",
                "pattern": "https://.+"
            }
        },

        "icon": {
            "type": "string",
            "title": "Path to 192x192 icon"
        },

        "appleIcon": {
            "type": "string",
            "title": "Path to 180x180 icon"
        },

        "manifestIcons": {
            "type": "array",
            "minItems": 1,
            "title": "Icons from webmaniifest file",
            "additionalItems": false,
            "items": {
                "type": "object",
                "title": "The icon schema",
                "additionalProperties": false,
                "required": [
                    "src",
                    "type",
                    "sizes"
                ],
                "properties": {
                    "src": {
                        "type": "string",
                        "title": "Path to icon"
                    },
                    "type": {
                        "type": "string",
                        "title": "MIME-type of icon"
                    },
                    "sizes": {
                        "type": "string",
                        "title": "Applicable sizes"
                    },
                    "purpose": {
                        "type": "string",
                        "title": "How the icon should be used (maskable/any)"
                    }
                }
            }
        },

        "tracks": {
            "type": "array",
            "minItems": 1,
            "title": "The list of tracks",
            "additionalItems": false,
            "items": {
                "type": "object",
                "title": "The track schema",
                "required": [
                    "page",
                    "duration",
                    "audioBasename"
                ],
                "properties": {
                    "page": {
                        "type": "string",
                        "title": "The page slug"
                    },
                    "instrumental": {
                        "type": "boolean",
                        "title": "The track is instrumental; affects visualizer",
                        "default": false
                    },
                    "duration": {
                        "type": "number",
                        "title": "The duration of the track"
                    },
                    "audioBasename": {
                        "type": "string",
                        "title": "The basename of audio file without extension",
                        "description": "The final URL of file will look as <mirror url>/<audioBasename>.<extension>"
                    },
                    "spotify": {
                        "type": "string",
                        "title": "Spotify ID",
                        "description": "Part after https://open.spotify.com/track/ url"
                    },
                    "amazon": {
                        "type": "string",
                        "title": "Amazon Standard Identification Number",
                        "description": "Part after https://www.amazon.com/dp/ url"
                    },
                    "youtube": {
                        "type": "string",
                        "title": "YouTube ID",
                        "description": "Part after https://www.youtube.com/watch?v= url"
                    },
                    "appleMusic": {
                        "type": "string",
                        "title": "Apple Music ID",
                        "description": "Part after https://geo.music.apple.com/album/id url"
                    },
                    "yandexMusic": {
                        "type": "string",
                        "title": "Yandex Music ID",
                        "description": "Part after https://music.yandex.ru/album/ url"
                    },
                    "bilibili": {
                        "type": "string",
                        "title": "Bilibili audio id",
                        "description": "Part after https://www.bilibili.com/audio/ url"
                    },
                    "flac": {
                        "type": "string",
                        "title": "Flac version",
                        "description": "Full link to download flac version"
                    }
                },
                "additionalProperties": false
            }
        }
    },
    "additionalProperties": false
}