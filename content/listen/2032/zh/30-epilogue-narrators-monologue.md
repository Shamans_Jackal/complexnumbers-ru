---
title: 收场白
subtitle: 叙述者的独白
translation: https://www.bilibili.com/audio/au2051563
translator: Elizabeth
byArtist:
- name: Denis Shamrin
  role: 叙述者
  image: /images/photos/den_1.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---

<div class="poem">

**叙述者**

所以，现在，我们知道，在这个世界上，<br>
&emsp;&emsp;&emsp;宿命论没有立足之地：<br>
事实上，世界的运行很复杂，<br>
&emsp;&emsp;&emsp;比行星的运动要难预测的多。

我们只能预测可能性，<br>
&emsp;&emsp;&emsp;一切都是由偶然性决定的<br>
在所有可能的未来中，<br>
&emsp;&emsp;&emsp;只有一个会成真…

</div>

