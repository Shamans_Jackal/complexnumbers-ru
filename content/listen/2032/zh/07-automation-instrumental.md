---
title: 自动化
subtitle: 纯音乐
image: /listen/2032/cover-clean.jpg
translation: https://www.bilibili.com/audio/au1831436
translator: Elizabeth
byArtist:
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
