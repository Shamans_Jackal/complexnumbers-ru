---
title: 两个月
subtitle: 叙述者的咏叹调
translation: https://www.bilibili.com/audio/au2035027
translator: Elizabeth
byArtist:
- name: Elena Yakovets
  role: 叙述者
  image: /images/photos/len_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
<div class="poem">

**叙述者**

两个月已经过去<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;在克里姆林宫的围墙里，<br>
&emsp;&emsp;&emsp;领袖们的争执<br>
和变革之风<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;都暂时停顿了下来，<br>
&emsp;&emsp;&emsp;因为统治者确信<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;这是个严重的问题…

&emsp;&emsp;&emsp;她管理着一切，<br>
&emsp;&emsp;&emsp;和以前一样。<br>
因此很难做出这个决定，<br>
&emsp;&emsp;&emsp;因为大家都知道：<br>
&emsp;&emsp;&emsp;这一定会影响<br>
治理国家的效率…<br>

经过十三年的时间<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;她已经成长得<br>
&emsp;&emsp;&emsp;行为举止与我们如此相似<br>
人们很容易<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;对她产生爱慕之情<br>
&emsp;&emsp;&emsp;并为此嫉妒领导人们<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;就好像她是真正的人…
</div>