---
title: 政变
translation: https://www.bilibili.com/audio/au2034942
translator: Elizabeth
byArtist:
- name: Ariel
  role: ASGU/全国自动化控制系统
  image: /images/photos/ari_1.jpg
- name: Vitaliy Trofimenko
  role: 阿纳托利·米利涅夫斯基
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---

**<u>[在米利涅夫斯基的房间]</u>**

*&lt;信息信号>*

**ASGU：** 很抱歉这么晚打扰你…
请使用手动控制，以免吵醒别人。

**米利涅夫斯基：** ……发生什么事了？…

**ASGU：** 我有个紧急消息。
伊朗社会党被…废黜。
就现在，莫斯科时间凌晨2点…

**米利涅夫斯基：** 哇…

**ASGU：** 他们已经宣布要离开邦联了。
发生了军事政变。

**米利涅夫斯基：** 你通知国防部长了吗？

**ASGU：** 我待会通知。
问题是，伊朗军队有个令人困惑的问题。
不清楚它支持哪边。

**米利涅夫斯基：** 你有足够的能力镇压叛乱吗？

**ASGU：** 我还是没有足够的终端。
尽管他们答应了我很多，但仍有很多未完成的部分。
我想过我们的34军，但是…

**米利涅夫斯基：** 是的，我明白。
这样做有些越权。
西方的反应是什么？
第二个阿富汗？
好的。
准备好军队，通知所有需要通知的人。
开始部署机器人，这需要时间。
如果有可能的话，最好从伊朗撤侨。
我们等几个小时吧。
我们明天早上8点再谈。
也许事情会好起来，我会好好睡一觉。
