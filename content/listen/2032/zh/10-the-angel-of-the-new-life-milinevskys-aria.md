---
title: 新生的天使
subtitle: 米利涅夫斯基的咏叹调
translation: https://www.bilibili.com/audio/au1849138
translator: Elizabeth
byArtist:
- name: Vitaliy Trofimenko
  role: 阿纳托利·米利涅夫斯基
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
<div class="poem">

**米利涅夫斯基**

&emsp;&emsp;&emsp;我一直都觉得<br>
&emsp;&emsp;&emsp;我们的目标很明确<br>
我们知道我们在追寻门后的什么<br>
&emsp;&emsp;&emsp;谎言也是正当的<br>
&emsp;&emsp;&emsp;在我们需要它时<br>
为了我们翘首以盼的成果<br>
&emsp;&emsp;&emsp;在人民的生命<br>
&emsp;&emsp;&emsp;和他们热情的驱动下<br>
使命将我们引向目标<br>
&emsp;&emsp;&emsp;所以，怎么可能<br>
&emsp;&emsp;&emsp;事实上，我们的目标<br>
现在被我们创造的神否认？

可是你<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;生来就<br>
&emsp;&emsp;&emsp;要成为我们新生的天使<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;你的清泉<br>
引导着我们<br>
&emsp;&emsp;&emsp;去往未来的日出<br>

如果责怪你<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;现在会显得很奇怪<br>
&emsp;&emsp;&emsp;你已经懂得太多<br>
可我们要如何走过<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;这条道路，同时避免你所谓<br>
&emsp;&emsp;&emsp;必须做出的牺牲？

&emsp;&emsp;&emsp;生活在我们的世纪并不容易<br>
&emsp;&emsp;&emsp;它充满了矛盾冲突<br>
有时候，快乐只是一种幻觉<br>
&emsp;&emsp;&emsp;或许，你也会<br>
&emsp;&emsp;&emsp;给我们快乐的幻觉<br>
将更重要的事藏在幕后<br>
&emsp;&emsp;&emsp;因为，如果你是对的<br>
&emsp;&emsp;&emsp;而且技术的进步<br>
不再需要我们天真的期望<br>
&emsp;&emsp;&emsp;那么，我们唯一的目的<br>
&emsp;&emsp;&emsp;就是个愚蠢的停滞社会<br>
其中大部分人仅仅是消费者！

可是你<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;生来就<br>
&emsp;&emsp;&emsp;要成为我们新生的天使<br>
&emsp;&emsp;&emsp;你的清泉<br>
引导着我们<br>
&emsp;&emsp;&emsp;去往未来的日出

那一天终将到来<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;那时你将会<br>
&emsp;&emsp;&emsp;重新理解这一切<br>
那一天终将到来<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;而我们将告诉自己<br>
&emsp;&emsp;&emsp;我们的期望是正确的！
</div>