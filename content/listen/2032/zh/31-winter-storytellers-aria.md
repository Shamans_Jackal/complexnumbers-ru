---
title: 冬天
subtitle: 叙述者的咏叹调
translation: https://www.bilibili.com/audio/au2051610
translator: Elizabeth
byArtist:
- name: Elena Yakovets
  role: 叙述者
  image: /images/photos/len_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
<div class="poem">

**叙述者**

冬天！...<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;在暗夜中，<br>
&emsp;&emsp;&emsp;凛冬笼罩着地上沉睡的城市，<br>
&emsp;&emsp;&emsp;在灰色苍穹下，似白烟飘过<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;她降临的太过仓促<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;只是个偶然的结果，<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;还是命运？<br><br>

世界<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;毫无生机<br>
&emsp;&emsp;&emsp;几百年前和昨日别无二致<br>
&emsp;&emsp;&emsp;它真的在意一个死去的花园吗？<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;在无数的行星中；<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;它只是一道流星<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;划过黑暗纪年？...<br><br>

或许，我们犯了个错误<br>
&emsp;&emsp;&emsp;在实现童真梦想的盲目竞赛中<br>
或许，我们生的太晚<br>
&emsp;&emsp;&emsp;在这没有直接道路的世界上？...<br><br>

冬天。<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;世界死了。<br>
&emsp;&emsp;&emsp;已经无人追究犯下罪恶的人；<br>
&emsp;&emsp;&emsp;无话询问从天空坠落的鸟儿：<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;只能责怪命运<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;导致了我们的结局，<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;这结局...<br><br>

让<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;雪烬落下<br>
&emsp;&emsp;&emsp;埋葬已经被焚尽年代的梦想<br>
&emsp;&emsp;&emsp;扫去曾经可笑的虚假耀眼胜利<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;和人类一切未竟的希望，<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;夜晚时模糊的希冀，<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;泡影...<br><br>

或许一切都将再次发生<br>
&emsp;&emsp;&emsp;或许悲剧不会重演<br>
鸟儿会再次飞在空中<br>
&emsp;&emsp;&emsp;但这世界已不属于我们：

我们急于求成，铸成大错<br>
&emsp;&emsp;&emsp;在为实现童真梦想的盲目竞赛中<br>
我们永远消失在那，<br>
&emsp;&emsp;&emsp;雪线后的白色火焰中...
</div>
