---
title: 200分钟
subtitle: 叙述者的咏叹调
translation: https://www.bilibili.com/audio/au2032491
translator: Elizabeth
byArtist:
- name: Elena Yakovets
  role: 叙述者
  image: /images/photos/len_2.jpg
- name: Aleksandr Asinksy
  role: 吉他伴奏
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
<div class="poem">

**叙述者**

只要200分钟，<br>
&emsp;&emsp;&emsp;列车就经过1100公里，<br>
苏联的又一天<br>
&emsp;&emsp;&emsp;与昏红的天空一同消失。<br>
今天发生的一切<br>
&emsp;&emsp;&emsp;也将消失，不再重要，<br>
今天的暗生情愫，<br>
&emsp;&emsp;&emsp;只会消散在记忆之中。

&emsp;&emsp;&emsp;可，我们无法爱这行星上<br>
所有渴望着被爱的人们。<br>
&emsp;&emsp;&emsp;无法触碰到他们的手<br>
在这童话里。

我们原本无法<br>
&emsp;&emsp;&emsp;感受到他人的喜怒哀乐，<br>
我们只关心自己，<br>
&emsp;&emsp;&emsp;除此以外一切都不在意，<br>
这样的自私自利<br>
&emsp;&emsp;&emsp;是我们生命的严重问题，<br>
然而社会的进步，<br>
&emsp;&emsp;&emsp;却依然还没有发现出路。

&emsp;&emsp;&emsp;这是千万人面临的悖论，<br>
多情善感无非成为负荷，<br>
&emsp;&emsp;&emsp;情感只在封闭单子之间<br>
走向湮灭。

&emsp;&emsp;&emsp;现在这梦想太不切实际：<br>
爱世界上所有渴求爱的人<br>
&emsp;&emsp;&emsp;永久的春天仍未降临在<br>
这童话里。
</div>