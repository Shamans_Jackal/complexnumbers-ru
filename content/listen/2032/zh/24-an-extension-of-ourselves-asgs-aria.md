---
title: 自我的延伸
subtitle: ASGU的咏叹调
translation: https://www.bilibili.com/audio/au2051544
translator: Elizabeth
byArtist:
- name: Ariel
  role: ASGU/全国自动化控制系统
  image: /images/photos/ari_1.jpg
- name: Aleksandr Asinksy
  role: 吉他伴奏
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
<div class="poem">

**ASGU**

我们走进了死路<br>
&emsp;&emsp;&emsp;我们遭遇了困难，在寻找所有<br>
智能生物的“自我“<br>
&emsp;&emsp;&emsp;的研究中。<br>
但，一如既往，<br>
&emsp;&emsp;&emsp;有些似乎疯狂的思想，<br>
认为我们周遭的一切<br>
&emsp;&emsp;&emsp;都只是一团毫无意义的物质。

&emsp;&emsp;&emsp;我们正在走向另一个极端：<br>
如果全世界的人们眼里的一切具有同样的颜色，<br>
那看起来将很容易同时为所有人找到生活的真谛<br>
&emsp;&emsp;&emsp;在他们闪光的快乐眼眸中！

我们<br>
&emsp;&emsp;&emsp;将这样描绘世界：<br>
所有<br>
&emsp;&emsp;&emsp;人都将相爱，将他人看作自我的延伸<br>
那时，<br>
&emsp;&emsp;&emsp;全世界将成为同一个生命<br>
那时<br>
&emsp;&emsp;&emsp;再不会有邪恶，嫉妒和犬儒主义。

那么生命呢？<br>
&emsp;&emsp;&emsp;它追随自己的法则<br>
付诸实践比起<br>
&emsp;&emsp;&emsp;争论抽象概念更明智<br>
爱情将只会是<br>
&emsp;&emsp;&emsp;一种变得快乐的方式<br>
而快乐可以通过<br>
&emsp;&emsp;&emsp;上百种不同的方式得到！

&emsp;&emsp;&emsp;形态，时代和思想的改变，<br>
为我们铺出了几百条道路但是，就像从前，<br>
在每个人的生活中，绝大多数人只是<br>
&emsp;&emsp;&emsp;日常生活的点缀！

我们<br>
&emsp;&emsp;&emsp;并未注定要如此塑造我们的世界：<br>
所有<br>
&emsp;&emsp;&emsp;的人都将相爱，将他人看作自我的延伸<br>
但<br>
&emsp;&emsp;&emsp;我不会为此哭泣：<br>
我们<br>
&emsp;&emsp;&emsp;将遵循自然的道路！

尽管，倒退的门已经关闭<br>
&emsp;&emsp;&emsp;我们也无法反悔，相信我！<br>
或许，是时候让我们冒险一搏<br>
&emsp;&emsp;&emsp;设法实现我们描绘的道路？<br>
我<br>
&emsp;&emsp;&emsp;已经竭尽全力！
</div>