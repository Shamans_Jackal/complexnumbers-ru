---
title: 我们的不可能之路
subtitle: 米利涅夫斯基的咏叹调
translation: https://www.bilibili.com/audio/au2051565
translator: Elizabeth
byArtist:
- name: Vitaliy Trofimenko
  role: 阿纳托利·米利涅夫斯基
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
<div class="poem">

**米利涅夫斯基**

死后我们会怎样？<br>
&emsp;&emsp;&emsp;只是无梦的长眠<br>
只是一次昏迷，那时<br>
&emsp;&emsp;&emsp;既没有幸福，也没有眼泪<br>
那时无尽的岁月<br>
&emsp;&emsp;&emsp;将转瞬即逝<br>
或许根本不会存在<br>
&emsp;&emsp;&emsp;于这里，现实中？

可不可能，我们<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;会因为自己的渴望死去，<br>
&emsp;&emsp;&emsp;会因为自己的梦想死去。<br>
&emsp;&emsp;&emsp;就像夸父追日一样徒劳？<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;如此像西西弗斯般反复？

我们能找到<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;心中的答案吗，从这场战争中？<br>
&emsp;&emsp;&emsp;我们能走完<br>
&emsp;&emsp;&emsp;我们的不可能之路吗<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;在地上人间？

为什么我们珍视世界？<br>
&emsp;&emsp;&emsp;我们从感觉得到了什么？<br>
因为世界并不总是<br>
&emsp;&emsp;&emsp;像一场美好的梦境。<br>
或许，她是对的：<br>
&emsp;&emsp;&emsp;一切的意义只在于快乐，<br>
那么，如果世界充满痛苦<br>
&emsp;&emsp;&emsp;就是时候扬弃它了？<br>

可不可能，我们<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;正在本无一物之处徒寻秘密？<br>
&emsp;&emsp;&emsp;对于“什么是生命？”<br>
&emsp;&emsp;&emsp;和“什么是价值？”<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;我曾被告知简单的答案<br>
但我们能<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;在黑暗中找到那虚幻的光明吗？<br>
&emsp;&emsp;&emsp;我们能走完<br>
&emsp;&emsp;&emsp;我们的不可能之路吗<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;在迷雾般的未来？

我要怎么才能相信<br>
&emsp;&emsp;&emsp;我再也不会见到你？<br>
事态又怎么会使得<br>
&emsp;&emsp;&emsp;我再也不能见你一面？！<br>
曾经，我难以想象<br>
&emsp;&emsp;&emsp;我会严肃对待这奇异的相似<br>
但我已经梦幻般堕入爱河<br>
&emsp;&emsp;&emsp;好像奇迹真的存在！

沉重的雨点<br>
&emsp;&emsp;&emsp;落下形成一面冰冷的墙<br>
列车安静的行进<br>
&emsp;&emsp;&emsp;在黑色的磁钢轨道上<br>
灯光不断向后飞入<br>
&emsp;&emsp;&emsp;疯狂竞赛的夜晚，这已不再重要<br>
世界为什么存在<br>
&emsp;&emsp;&emsp;为什么我们有一天会出现。

充满痛苦的世界<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;不能再一如既往的存在<br>
&emsp;&emsp;&emsp;它必须要改变，<br>
&emsp;&emsp;&emsp;否则就丝毫不<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;值得留念。<br>
地上的磅礴大雨<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;从我们的记忆中洗刷了过去的尘埃<br>
&emsp;&emsp;&emsp;我们必须抵达<br>
&emsp;&emsp;&emsp;我们的不可能之路的终点，无论怎样。<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;世界在我们面前冻结了…
</div>
