---
title: 斗转星移之间
subtitle: 叙述者的咏叹调
translation: https://www.bilibili.com/audio/au1822730
translator: Elizabeth
byArtist:
- name: Elena Yakovets
  role: 叙述者
  image: /images/photos/len_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
<div class="poem">

**叙述者**

在斗转星移之间，<br>
&emsp;&emsp;&emsp;绚烂的梦想浮动，<br>
有些夙愿终实现，<br>
&emsp;&emsp;&emsp;有些消失在虚空，<br>
也有千年之梦光彩夺目引人流连，<br>
&emsp;&emsp;&emsp;却难以企及，难道她永远只是个梦？.

但前途越是千难万险，<br>
&emsp;&emsp;&emsp;她越令人怦然心动。<br>
我们相信梦想将实现，<br>
&emsp;&emsp;&emsp;对旧世界无动于衷。

仍然坚守信念，<br>
&emsp;&emsp;&emsp;脚下是万丈深渊。<br>
我们筚路蓝缕，遗忘了这谎言：<br>
这蔚蓝世界归我们独占，只是因为<br>
&emsp;&emsp;&emsp;我们可以为理想毁掉它，在顷刻之间。

啊也许，这样愚不可及<br>
&emsp;&emsp;&emsp;宣告我们至高的梦<br>
她就像是天真的童话，<br>
&emsp;&emsp;&emsp;但值得用一生实现！

在斗转星移之间，<br>
&emsp;&emsp;&emsp;对涌现出的观念，<br>
历史无情审判，<br>
&emsp;&emsp;&emsp;也公正明镜高悬，<br>
太阳城的理念已经历经沧海桑田，<br>
&emsp;&emsp;&emsp;却还未成真，仍然只是南柯一梦…

但前途越是千难万险，<br>
&emsp;&emsp;&emsp;她越令人怦然心动，<br>
她的光明刺穿旧世界，<br>
&emsp;&emsp;&emsp;在长夜中拥吾入眠。

啊也许，这样愚不可及<br>
&emsp;&emsp;&emsp;宣告我们至高的梦<br>
她就像是天真的童话，<br>
&emsp;&emsp;&emsp;但值得用一生实现！
</div>
