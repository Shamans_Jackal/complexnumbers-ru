---
title: The Angel of New Life
subtitle: Milinevsky's aria
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-10-%E2%80%93-%D0%B0%D0%BD%D0%B3%D0%B5%D0%BB-%D0%B6%D0%B8%D0%B7%D0%BD%D0%B8-%D0%BD%D0%BE%D0%B2%D0%BE%D0%B9-2032-track-10-%E2%80%93-angel-ne.html
translator: FireyFlamy
byArtist:
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Milinevsky**

&emsp;&emsp;&emsp;It has always seemed to me<br>
&emsp;&emsp;&emsp;That our goal is clear<br>
And we know what we’re searching for behind the threshold<br>
&emsp;&emsp;&emsp;And a lie is justified, too,<br>
&emsp;&emsp;&emsp;At times when we need it<br>
For the sake of the sought-for results<br>
&emsp;&emsp;&emsp;And with the help of people’s lives<br>
&emsp;&emsp;&emsp;And the motives of their doings,<br>
The destiny itself guides us to the goal<br>
&emsp;&emsp;&emsp;So, how could it be that<br>
&emsp;&emsp;&emsp;In fact, this very goal<br>
Is now denied by our artificial god?

But you were<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Created<br>
&emsp;&emsp;&emsp;To be our angel of new life<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;The spring of yours<br>
Has invited us<br>
&emsp;&emsp;&emsp;To the sunrise of the upcoming days!..<br>

It would have been strange<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;To blame you now:<br>
&emsp;&emsp;&emsp;You already know too much<br>
But how can we follow<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;This path without losses<br>
&emsp;&emsp;&emsp;That are meant to happen, as you say?

&emsp;&emsp;&emsp;Life in our century isn’t simple<br>
&emsp;&emsp;&emsp;It’s full of contradictions:<br>
Sometimes, happiness is just an illusion,<br>
&emsp;&emsp;&emsp;And probably, you will<br>
&emsp;&emsp;&emsp;Give us illusions, too,<br>
Leaving something important out of sight<br>
&emsp;&emsp;&emsp;Because, if you are right<br>
&emsp;&emsp;&emsp;And the technical progress<br>
No longer needs our naïve aspirations,<br>
&emsp;&emsp;&emsp;Then, now, our only goal<br>
&emsp;&emsp;&emsp;Is a stupid satisfied society<br>
Where most people are just consumers!

But you were<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Created<br>
&emsp;&emsp;&emsp;To be our angel of new life<br>
&emsp;&emsp;&emsp;The spring of yours<br>
Has guided us<br>
&emsp;&emsp;&emsp;To the dreams of our terrestrial path!..

And the day will come<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;And you’ll suddenly<br>
&emsp;&emsp;&emsp;Understand everything once again<br>
And the day will come<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;And we’ll tell ourselves:<br>
&emsp;&emsp;&emsp;Our aspirations are worth it!
</div>