---
title: The Results of People's Love
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-11-%E2%80%93-%D0%BF%D0%BB%D0%BE%D0%B4%D1%8B-%D0%BB%D1%8E%D0%B1%D0%B2%D0%B8-%D0%BD%D0%B0%D1%80%D0%BE%D0%B4%D0%BD%D0%BE%D0%B9-2032-track-11-%E2%80%93-resul.html
translator: FireyFlamy
byArtist:
- name: Denis Shamrin
  role: Narrator, Delegate
  image: /images/photos/den_1.jpg
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Elena Yakovets
  role: Emcee
  image: /images/photos/len_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

**Narrator:**

It’s just necessary for charismatic politicians<br>
&emsp;&emsp;&emsp;To be close to people nowadays<br>
Even though, sometimes, it can be hard<br>
&emsp;&emsp;&emsp;To take the results of people’s love seriously

But neither television, nor the Net<br>
&emsp;&emsp;&emsp;Is able to replace human interaction in real life<br>
And to get rid of all the visits and meetings<br>
&emsp;&emsp;&emsp;People themselves are so hard to change.

**<u>[At the ExCom‘s auditorium, general view]</u>**

*&lt;applause>*

**Emcee:** Right after the 12th school of our city, the warm greetings to our wonderful leader comrade Milinevsky are brought by the pupils of the secondary school number 8!

*&lt;applause>*

**<u>[At the ExCom‘s auditorium, close view]</u>**

**Milinevsky:** Listen, when shall we get back to Moscow?

**Delegate:** ASGU said she’d bring the train in the evening.
She didn’t decide on the exact time, this is a closed city…

**<u>[At the ExCom‘s auditorium, general view]</u>**

**Emcee:** Dear Anatoly Sergeevich!
You’re about to hear the performance of the pupil of the 11th class “a” Limaeva Svetlana.
She has dedicated this song to our great party, our government, and to you personally!

*&lt;microphone's click>*