---
title: The Great Hour
subtitle: Milinevsky's aria
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-21-%E2%80%93-%D0%B2%D0%B5%D0%BB%D0%B8%D0%BA%D0%B8%D0%B9-%D1%87%D0%B0%D1%81-2032-track-21-%E2%80%93-great-hour.html
translator: FireyFlamy
byArtist:
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Milinevsky**

The history has swept over us just like a wave<br>
It was the thirteen-years-long calm before the storm<br>
And yet we haven’t made the world order<br>
The stable structure in the way we want it to be.
 
We’ve been challenged, and I doubt that<br>
We should stop, leaving everything as it is:<br>
If it’s impossible to avoid the great war now,<br>
Then there’s no time for nice gestures!
 
I see, the great hour has come<br>
And the outbreak of a multilocal conflict<br>
Might eventually<br>
Turn into the world revolution!
 
But even if all these thoughts are total nonsense,<br>
The war can give us a chance of even faster development,<br>
So, let the red sunrise shine<br>
Over this old world, just like a flag!
</div>