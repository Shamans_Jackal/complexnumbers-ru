---
title: Two Months
subtitle: Narrator's aria
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-17-%E2%80%93-%D0%B4%D0%B2%D0%B0-%D0%BC%D0%B5%D1%81%D1%8F%D1%86%D0%B0-2032-track-17-%E2%80%93-two-months.html
translator: FireyFlamy
byArtist:
- name: Elena Yakovets
  role: Narrator
  image: /images/photos/len_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Narrator**

Two months has passed since then,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;And now, within the ring of Kremlin’s walls,<br>
&emsp;&emsp;&emsp;There’s no disputes in the corridors of power<br>
And yet the wind of change<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Is held back for some time<br>
&emsp;&emsp;&emsp;By the people, who regarded it<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;To be a great problem…

&emsp;&emsp;&emsp;She had to control everything<br>
&emsp;&emsp;&emsp;Just like she has now, though<br>
And that’s why that very decision was even more difficult:<br>
&emsp;&emsp;&emsp;Everybody knew that it<br>
&emsp;&emsp;&emsp;Shouldn’t had been made<br>
Because it could have lowered the efficiency of governance…<br>

By her thirteen years,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;She has been able to become<br>
&emsp;&emsp;&emsp;Someone surprisingly similar to us, occasionally.<br>
And it was so easy<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;To fall in love with her<br>
&emsp;&emsp;&emsp;And to be jealous of authorities,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;As if she was a human…
</div>