---
title: The Choice
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-28-%E2%80%93-%D0%B2%D1%8B%D0%B1%D0%BE%D1%80-2032-track-28-%E2%80%93-choice.html
translator: FireyFlamy
byArtist:
- name: Ariel
  role: ASGU
  image: /images/photos/ari_1.jpg
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

**ASGU:** …So, you’ve agreed with me, after all…
The value of life… is relative.
That’d be a pity, though, if I was right… about that only…  
Oh, yeah… Humans…
On the both sides of the Earth…
How can we even talk historical materialism…
You still believe in a magical wand of radical decisions…
that it can substitute material and technical basis…
and, formally, bring the Christian heaven down…
to earth…

**Milinevsky.** My decision is firm.

**ASGU:** Well… Enter the code, then.
Just for the sake of formalities…
I really don’t care.

You’ve put me in my place.
