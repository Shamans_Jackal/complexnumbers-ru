---
title: Unrealizable Path
subtitle: Milinevsky's aria
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-27-%E2%80%93-%D0%BD%D0%B5%D1%81%D0%B1%D1%8B%D1%82%D0%BE%D1%87%D0%BD%D1%8B%D0%B9-%D1%81%D0%B2%D0%BE%D0%B9-%D0%BF%D1%83%D1%82%D1%8C-2032-track-27-%E2%80%93-our-unr.html
translator: FireyFlamy
byArtist:
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Milinevsky**

What awaits us after life?<br>
&emsp;&emsp;&emsp;Just a slow dream with no fantasies<br>
Just a syncope, where there’s<br>
&emsp;&emsp;&emsp;Neither happiness, nor tears.<br>
And where the endless years<br>
&emsp;&emsp;&emsp;Will pass in just a moment<br>
Which even might not exist<br>
&emsp;&emsp;&emsp;Here, in reality?

What, if we<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Will die because of our own aspirations,<br>
&emsp;&emsp;&emsp;Will die because of our dream,<br>
&emsp;&emsp;&emsp;That is running away from us just like horizon?<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Over and over again?

Shall we find<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;The answer inside of ourselves, in this war?<br>
&emsp;&emsp;&emsp;Shall we reach the end<br>
&emsp;&emsp;&emsp;Of our unrealizable path<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;In such terrestrial conditions?

Why do we value the world,<br>
&emsp;&emsp;&emsp;What do we get from our feelings?<br>
Because it doesn’t always seem<br>
&emsp;&emsp;&emsp;To be like a wonderful dream.<br>
Or, maybe, she is right:<br>
&emsp;&emsp;&emsp;The meaning of everything is in happiness only,<br>
And, if our world is just pain,<br>
&emsp;&emsp;&emsp;It’s time to get rid of it?<br>

What, if we<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Are searching for the secrets in places where there’s none?<br>
&emsp;&emsp;&emsp;As for the questions “What is life?”<br>
&emsp;&emsp;&emsp;And “what is its value?”,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;I was given a simple answer<br>
But shall we find<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;That illusive light in the dark?<br>
&emsp;&emsp;&emsp;Shall we reach the end<br>
&emsp;&emsp;&emsp;Of our unrealizable path<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;In the haze of the upcoming years?

How am I supposed to believe<br>
&emsp;&emsp;&emsp;That, from now on, I won’t see you again?<br>
How could it even be<br>
&emsp;&emsp;&emsp;That I’ll never ever see you again?!<br>
It’s unlikely that<br>
&emsp;&emsp;&emsp;I took that strange resemblance seriously back then<br>
But I’ve fallen in love just like in a dream<br>
&emsp;&emsp;&emsp;As if miracles really existed!

The heavy drops of rain<br>
&emsp;&emsp;&emsp;Are falling down forming a single cold wall<br>
And the train is quietly going further,<br>
&emsp;&emsp;&emsp;Moving over the black magnet steel line<br>
And the lights are flying<br>
&emsp;&emsp;&emsp;Into the night in their mad race, and it doesn’t matter anymore<br>
Why does this world around us exist<br>
&emsp;&emsp;&emsp;Why we’ve even appeared there one day

The world that has pain<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;No longer has the right to live as it did before:<br>
&emsp;&emsp;&emsp;It has to become different,<br>
&emsp;&emsp;&emsp;Or it’s not worth<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Keeping it alive.<br>
The rain over earth<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Has washed away the dust of the past from our memory<br>
&emsp;&emsp;&emsp;We shall reach the end<br>
&emsp;&emsp;&emsp;Of our unrealizable path, no matter what<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;The world has frozen in front of us…
</div>
