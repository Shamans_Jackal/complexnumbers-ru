---
title: The Results of a Controversial Approach
subtitle: Milinevsky's aria
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-06-%E2%80%93-%D0%BF%D0%BB%D0%BE%D0%B4%D1%8B-%D0%BD%D0%B5%D0%BE%D0%B4%D0%BD%D0%BE%D0%B7%D0%BD%D0%B0%D1%87%D0%BD%D0%BE%D0%B3%D0%BE-%D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4%D0%B0-2032-track.html
translator: FireyFlamy
byArtist:
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Milinevsky**

&emsp;&emsp;&emsp;It’s been just four years,<br>
But how much everything has changed!<br>
How fast your flower meadow has given us<br>
&emsp;&emsp;&emsp;The results of a controversial approach!..

&emsp;&emsp;&emsp;Everything is so easy for you: with no fervor,<br>
Each and every question is crystal clear,<br>
And the terrestrial world is so perfectly beautiful,<br>
&emsp;&emsp;&emsp;There’s no worry about the passing days

&emsp;&emsp;&emsp;And you know nothing about<br>
Doubts, hesitation and shame,<br>
And you know neither offence,<br>
&emsp;&emsp;&emsp;Nor regrets for each decision

&emsp;&emsp;&emsp;And you seem to know the answer<br>
To the questions we’ve been thinking about for our entire lives<br>
But you’ve managed to ascend so fast!<br>
&emsp;&emsp;&emsp;Should we really be trusting you?

And what about automation?.. It’s a natural process.<br>
&emsp;&emsp;&emsp;And now it takes place across the entire planet!<br>
So, does it turn out that the 100-year struggle was meaningless?<br>
&emsp;&emsp;&emsp;All the revolutions, and the great victories?

But, maybe we do have a goal – the machine communism,<br>
&emsp;&emsp;&emsp;And perhaps the labour is no longer what it used to be?<br>
And our collectivism is just an atavism<br>
&emsp;&emsp;&emsp;That exists only because of the underdeveloped production models?!
</div>
