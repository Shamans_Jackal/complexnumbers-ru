---
title: The Hospital
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-25-%E2%80%93-%D1%81%D1%82%D0%B0%D1%86%D0%B8%D0%BE%D0%BD%D0%B0%D1%80-2032-track-25-%E2%80%93-hospital.html
translator: FireyFlamy
byArtist:
- name: Ariel
  role: Svetlana Limaeva
  image: /images/photos/ari_1.jpg
- name: Sergey Goncharov
  role: Doctor in charge
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

**<u>[At the hospital’s cabinet of the doctor in charge]</u>**

**Doctor in charge:** Good evening!
Frankly speaking, we haven’t been aware of your upcoming visit.
I’ve got to know only 5 minutes ago…
The visit of a deputy is such an honor for us!
Also, you look a lot like…

**Milinevsky:** I’m here because of an informal issue.

**Doctor in charge:** I… I’m listening!

**Milinevsky:** Is Limaeva Svetlana Aleksandrovna in this department?

**Doctor in charge:** Yes, she is here.

**Milinevsky:** I’d like to see her.

**Doctor in charge:** You know…
She’s in the room of intensive therapy right now.
She is unconscious.

**Milinevsky:** Unconscious?!
What happened?

**Doctor in charge:** She’s in a critical condition.
Well, you know, it’s a really strange case, actually.
The result of a prolonged stress…
The nervous exhaustion.
Maybe, she had some really intense experience, because of unhappy love or something…
She doesn’t talk about it, though.
Her parents are either not aware of that, or just remain silent, too…

**Milinevsky:** I must see her!

**Doctor in charge:** This is out of the question.
Only personnel can go there, and even them..

**Milinevsky:** I’ve got a very serious issue.
I can’t talk about it.
I didn’t want to tell anything at all, but here is the document from the Central Committee.

**Doctor in charge:** Oh, my god…
Why’d they even need that…
Fine, follow me.

**<u>[At the room of intensive therapy]</u>**

**Limaeva:** It is you…

**Doctor in charge:** I’ll leave you together.
Everything can be seen on the devices…

**Limaeva:** I knew you’d come…
But it’s too late now, I guess?

**Milinevsky:** Why?

**Limaeva:** Well, I’ll die soon anyway.
It’s just meant… to be…

**Milinevsky:** What are you talking about!

**Limaeva:** But it’s okay, right?
Maybe, someday, in hundreds or thousands of years’ people will be able to bring every person that ever lived back to life, won’t they?
And, then, we’ll have a chance to be together.
Because communism is when every person loves everyone, isn’t it?
That is true, right?

**Milinevsky:** Right… It’s probably true…
But resurrection… All these pseudoreligious ideas…
It’s impossible!

**Limaeva:** I know it’s meant to be this way!
We’ll be together there!
Because we can’t be together now, and that’s why… I have to…

**Doctor in charge:** Excuse me, we have a problem!

*&lt;the periodic pulse signal becomes continuous>*

**Milinevsky:** No… No!!!

**Doctor in charge:** We’re losing her…
Natalia Gilmutdinovna, once again!

*&lt;the sound of devices>*

**Doctor in charge:** It doesn’t help…
Call Haritonov!

**Milinevsky:** What about cryonics??
Can you perform fast cryopreservation?
Is it too late?

**Doctor in charge:** Well, I think that’s still possible, but…

**Milinevsky:** The Central Committee will fund it!

**Doctor in charge:** Yes, I’m very grateful…
Of course, this technology isn’t run-in yet…
There’s no guaranty…

**<u>[At the porch of the hospital; it’s raining]</u>**

**Doctor in charge:** In any case, don’t blame yourself.
It was hopeless anyway, I just didn’t want to tell you.
Even though, when you entered the room, I thought that, maybe, she…
Well, thanks to you, there’s still hope, at least.
Maybe, after many years…
