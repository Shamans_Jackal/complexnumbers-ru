---
title: The Coup
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-18-%E2%80%93-%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D1%80%D0%BE%D1%82-2032-track-18-%E2%80%93-coup.html
translator: FireyFlamy
byArtist:
- name: Ariel
  role: ASGU
  image: /images/photos/ari_1.jpg
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

**<u>[At Milinevsky’s room]</u>**

*&lt;the message signal>*

**ASGU:** I’m sorry for disturbing you that late…
Take the manual control, so that not to wake anyone up.

**Milinevsky:** …What happened? …

**ASGU:** I’ve got an emergency message.
The Iran Socialist Party was… deposed.
Right now, at 2 AM, Moscow time…

**Milinevsky:** Whoa…

**ASGU:** They’ve already declared they’re leaving the Confederation.
There’s a military coup.

**Milinevsky:** Have you notified the Defense Minister?

**ASGU:** I’ll do that later.
The problem here is, they’ve got a confusing issue with their army.
It’s unclear which side it is on.

**Milinevsky:** Do you have enough authority to suppress the mutiny?

**ASGU:** I still don’t have enough terminals there.
As much as they promised me, there’s still a lot to be desired.
I’ve thought about our 34th army, but...

**Milinevsky:** Right, I understand.
Such a step would take too much responsibility.
And how’d the West answer this?
The second Afghanistan?..
Fine.
Prepare the army, notify everyone needed.
Start getting robots into position, it can’t be done quickly.
If there’s a possibility, it’s better to evacuate our people from Iran.
And let’s wait for a few hours.
We’ll talk tomorrow at 8 AM.
Maybe things will clear up, and I’ll get a good sleep.
