---
title: I'm Sorry!..
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-13-%E2%80%93-%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%B8%D1%82%D0%B5-2032-track-13-%E2%80%93-im-sorry.html
translator: FireyFlamy
byArtist:
- name: Ariel
  role: Svetlana Limaeva
  image: /images/photos/ari_1.jpg
- name: Denis Shamrin
  role: Delegate
  image: /images/photos/den_1.jpg
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

**<u>[At the ExCom‘s auditorium, general view]</u>**

*&lt;applause>*

**Limaeva.** I’m sorry!..

**<u>[At the ExCom‘s auditorium, close view]</u>**

**Milinevsky:** What the heck… She fainted!..

**Delegate:** Anatoly Sergeevich! I think that’s going to be a scandal.

**Milinevsky:** Well, I doubt that… But why does she have such a voice?

**Delegate:** What’s wrong with it?

**Milinevsky:** It’s ASGU’s voice…

**Delegate:** Well, yeah, it sounds very similar… Weird.

**Milinevsky:** Tell them I want to talk to her.
