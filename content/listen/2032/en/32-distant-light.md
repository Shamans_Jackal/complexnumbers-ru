---
title: Distant Light
subtitle: ASGU's / Limaeva's vocalise
image: /listen/2032/cover-clean.jpg
byArtist:
- name: Ariel
  role: ASGU/Limaeva
  image: /images/photos/ari_1.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

