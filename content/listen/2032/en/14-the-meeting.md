---
title: The Encounter
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-14-%E2%80%93-%D0%B2%D1%81%D1%82%D1%80%D0%B5%D1%87%D0%B0-2032-track-14-%E2%80%93-encounter.html
translator: FireyFlamy
byArtist:
- name: Ariel
  role: Svetlana Limaeva
  image: /images/photos/ari_1.jpg
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

**<u>[At the ExCom‘s cabinet]</u>**

**Limaeva:** … Good evening! …

**Milinevsky:** Yes… Please, take a sit!..
..What can I say…
You came to the stage, performed that song… And what’s next?
Do you really think that’s going to work?
I have a family, after all, and…  
…By the way, don’t you know why your voice sounds familiar to me?

**Limaeva:** My… voice?

**Milinevsky:** Yes.  
…Okay, nevermind… It’s probably nothing…
You know… Stalin, Plotnikov, Romanov… Well, in fact, all of us received a certain number of love letters.
And me, too. Even though, I think nobody did it in such a way before.
Or, at least, I don’t know about it.  
Well, actually, I liked your song.
I really did.
I guess a technician just couldn’t turn the sound off when he realized that it wasn’t… the right thing.
How old are you?

**Limaeva:** I’m 16.

**Milinevsky:** Oh, right… You’re in the 11th grade already.  
Alright…
I’ve got to go now. There’s still a lot to do.
Maybe, we’ll meet again later.

**Limaeva:** Meet… again?..

**Milinevsky:** Why not?

**Limaeva:** I… I’ll be waiting for you!..
I’ll be waiting for you…
