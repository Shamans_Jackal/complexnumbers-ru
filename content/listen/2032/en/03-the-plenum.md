---
title: The Plenary
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-03-%E2%80%93-%D0%BF%D0%BB%D0%B5%D0%BD%D1%83%D0%BC-2032-track-03-%E2%80%93-plenary.html
translator: FireyFlamy
byArtist:
- name: Dmitry Gonchar
  role: Dictor
- name: Evgeny Grivanov
  role: First Radio Listener
- name: Denis Shamrin
  role: Second Radio Listener
  image: /images/photos/den_1.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

**<u>[An office at the factory]</u>**

**&lt;official time signal>**

**Dictor** 

Moscow’s speaking. Current Moscow time is 11 AM.

***&lt;news theme melody>***

We’ll start from the latest news.  
The plenary session of the Central Committee of the Soviet Union Communist Party was held yesterday.  
On behalf of the Party’s Politburo, the session was opened by the member of Politburo, the First Secretary of Leningrad Oblast CPSU Committee Grigory Vasilyevich Romanov.  
In connection with the death of the General Secretary of CC CPSU, the Chair of the Presidium of the Supreme Soviet of the USSR Konstantin Ustinovich Chernenko, the plenary participants had a minute of silence.  
The plenum noted the Soviet Union Communist Party and all the Soviet people have suffered a grievous loss. Konstantin Ustinovich was an outstanding party leader and statesman, a patriot and an internationalist, a remarkable activist who fought to ensure the triumph of the ideals of communism and peace on the Earth…

**First Radio Listener:** Hey, are they going to propose Romanov as a new CC CRSU General Secretary? I thought they were for Gorbachov…

**Second Radio Listener:** Well, Andropov proposed him, but he wasn't chosen…

**First Radio Listener:** What about Grishin?

**Second Radio Listener:** Grishin?.. That guy didn’t really want to run, himself…

**Dictor:** The plenary participants expressed their deep condolences to the family and friends of the late General Secretary. The CC plenary considered choosing a new CC CRSU General Secretary. On behalf of the Party’s Politburo, the member of Politburo and the Head of the Ministers Council comrade Tikhonov delivered a speech about the issue. He proposed to elect the First Secretary of the Leningrad Oblast Committee, comrade Romanov, as the new CC CRSU General Secretary. The plenum unanimously decided to elect Grigory Vasilyevich Romanov as the General Secretary of CC CPSU. Then the newly elected CC CRSU General Secretary comrade Romanov gave a speech…

**Narrator**

And life went on… The country was moving<br>
&emsp;&emsp;&emsp;Towards our great dream,<br>
Even though it was unclear whether we were getting close<br>
&emsp;&emsp;&emsp;To that distant, illusive line.

And years will pass, children will grow up,<br>
&emsp;&emsp;&emsp;The leader will be changed two more times,<br>
And, then, the unkind rain of gossips and doubts<br>
&emsp;&emsp;&emsp;Will finally stop, not being able to wash out the plinths.
