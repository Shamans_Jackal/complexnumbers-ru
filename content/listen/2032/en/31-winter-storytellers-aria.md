---
title: The Winter
subtitle: Narrator's aria
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-31-%E2%80%93-%D0%B7%D0%B8%D0%BC%D0%B0-2032-track-31-%E2%80%93-winter.html
translator: FireyFlamy
byArtist:
- name: Elena Yakovets
  role: Narrator
  image: /images/photos/len_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Narrator**

Winter!..<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;In the dark of the night,<br>
&emsp;&emsp;&emsp;Winter is floating over the sleeping earth city,<br>
&emsp;&emsp;&emsp;And under the gray skies, just like a white smoke<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;She came here too early<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;As a result of a simple causality,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Or was it our fate?..<br><br>

And the world<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Is almost the same<br>
&emsp;&emsp;&emsp;As it was yesterday and hundreds of years ago:<br>
&emsp;&emsp;&emsp;Does it really care about just one dead garden?<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Among the numerous planets,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;It’s just one vanished trace<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;In all the darkness of years?..<br><br>

Maybe, we made a mistake<br>
&emsp;&emsp;&emsp;In that blind race after the childhood dream<br>
Maybe, we were born too late<br>
&emsp;&emsp;&emsp;In this world, where there’s no direct path?..<br><br>

Winter.<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;The world is dead.<br>
&emsp;&emsp;&emsp;And now there’s no one to search for the guilty people…<br>
&emsp;&emsp;&emsp;And now there’s nothing to ask the birds that fell down from the skies:<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Only the circumstances are to blame<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;For everything that has turned into our destiny,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;This destiny…<br><br>

And let<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Snow fall down like ash<br>
&emsp;&emsp;&emsp;And bury those dreams of the burnt-out years,<br>
&emsp;&emsp;&emsp;Sweeping away the fake glitter of victories that now seem so ridiculous,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;And all the unfulfilled hopes of humanity,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;And those vague images of the night,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;The empty ones…<br><br>

Maybe everything will happen again<br>
&emsp;&emsp;&emsp;Maybe it won’t be as tragic as it is now<br>
The birds will fly in the sky again<br>
&emsp;&emsp;&emsp;But this world is no longer for us:
 
We hurried so much and made a mistake<br>
&emsp;&emsp;&emsp;In that blind race after the childhood dream<br>
We’ve vanished inside of it, for keeps,<br>
&emsp;&emsp;&emsp;In that white fire, behind the snow line…
</div>
