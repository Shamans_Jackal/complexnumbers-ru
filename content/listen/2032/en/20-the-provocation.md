---
title: Provocation
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-20-%E2%80%93-%D0%BF%D1%80%D0%BE%D0%B2%D0%BE%D0%BA%D0%B0%D1%86%D0%B8%D1%8F-2032-track-20-%E2%80%93-provocation.html
translator: FireyFlamy
byArtist:
- name: Ariel
  role: ASGU
  image: /images/photos/ari_1.jpg
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

**<u>[At Milinevsky’s room]</u>**

**Milinevsky:** By the way, don’t you know why your voice sounds so familiar to me?

**ASGU:** My… voice? That’s me!

**Milinevsky:** Ah, that’s right…
So, how are the things going?

**ASGU:** Well… the situation became… even more complicated.
Firstly, the Iran army not only supported the rebellion, but also invaded South Afghanistan’s territory.
We all know what their border is like.
I tried to shoot there, but… it’s useless.

**Milinevsky:** Wow!..

**ASGU:** That’s not all yet.
Two North Korean outposts were blown up at 3 AM, and at 5 AM they had a shooting at the sea, and it’s unclear who had started it.
