---
title: Зима
subtitle: ария Рассказчицы
byArtist:
- name: Елена Яковец
  role: Рассказчица
  image: /images/photos/len_2.jpg
- name: Виктор Аргонов
  role: Музыка, либретто, звук, аранжировка
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Рассказчица**

Зима!..<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Во тьме ночной<br>
&emsp;&emsp;&emsp;Кружится над уснувшим городом земным –<br>
&emsp;&emsp;&emsp;Под серым небосводом, словно белый дым.<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Она пришла сюда не в срок:<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Простой случайности итог,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Иль рок?..<br><br>

А мир –<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Почти такой,<br>
&emsp;&emsp;&emsp;Каким он был вчера и сотни лет назад:<br>
&emsp;&emsp;&emsp;Что значит для него один увядший сад –<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Среди бесчисленных планет<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Всего один угасший след<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Во мраке лет?..<br><br>

Может быть, мы где-то ошиблись<br>
&emsp;&emsp;&emsp;В гонке слепой за детской мечтой,<br>
Может быть, мы поздно родились<br>
&emsp;&emsp;&emsp;В мире, где нет дороги прямой?<br><br>

Зима.<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Окончен бег.<br>
&emsp;&emsp;&emsp;И некому искать вину конкретных лиц,<br>
&emsp;&emsp;&emsp;И не о чем спросить упавших с неба птиц:<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Лишь обстоятельства виной<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Тому, что стало всем судьбой<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Такой...<br><br>

И пусть<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Как пепел, снег<br>
&emsp;&emsp;&emsp;Ложится, засыпая сны сгоревших лет,<br>
&emsp;&emsp;&emsp;Сметая ложный блеск смешных теперь побед,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Надежд несбывшихся земных,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Неясных образов ночных...<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Пустых...<br><br>

Может, ещё всё повторится<br>
&emsp;&emsp;&emsp;Может счастливее, чем в этот раз –<br>
Вновь полетят по небу птицы,<br>
&emsp;&emsp;&emsp;Но этот мир уже не для нас:

Мы в суете где-то ошиблись<br>
&emsp;&emsp;&emsp;В гонке слепой за детской мечтой,<br>
Мы навсегда там растворились,<br>
&emsp;&emsp;&emsp;В белом огне за снежной чертой...
</div>