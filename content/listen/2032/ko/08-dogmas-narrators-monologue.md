---
title: 도그마
translation: http://transural.egloos.com/314286
translator: GRU Fields
byArtist:
- name: 데니스 샴린
  role: 나레이터
  image: /images/photos/den_1.jpg
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**나레이터**

우린 진리가 오직 언제나 우리들의 경험으로부터 나온다고 믿습니다.<br>
컴퓨터는 논리를 개념화 하고, 일반화 하며.

이데올로기는 이미 컴퓨터에 저장되어, 또 이념을 무장시킬 필요는 없습니다!<br>
모든 진리, 논리에 접근해,  우리가 예측한 것처럼 보여줄것입니다.

만약 현대적 기계두뇌가 위대한 이념추구를 거절하게 된다면<br>
어쩌면 예를 들어, 이념추구에 회의를 가진다면 말입니다.

그래도, 이념전사는 신을 두려워 하지 않듯<br>
거의 그럴리 없지만 - 컴퓨터가 곧 어긋나가게 될경우..

그런다 한들, 컴퓨터의 기억 회로(도그마)를 아주 가볍고 어렵지 않게 교체할수 있습니다.<br>
컴퓨터가 망가져 있지만 않으면 말입니다..!
</div>