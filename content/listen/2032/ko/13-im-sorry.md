---
title: 죄송합니다..
translation: http://transural.egloos.com/314511
translator: GRU Fields
byArtist:
- name: 아리엘
  role: 스베틀라나 라마예바
  image: /images/photos/ari_1.jpg
- name: 데니스 샴린
  role: 밀리넵스키의 비서
  image: /images/photos/den_1.jpg
- name: 빅토르 트로피멘코
  role: 아나톨리 밀리넵스키
  image: /images/photos/vit_2.jpg
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---

**스베틀라나:** 죄송합니다...

**밀리넵스키:** 세상에나 저게 뭐야? 기절해 버렸어!

**밀리넵스키의 보좌관:** 아나톨리 세르게예비치. 제 생각엔 정말 부끄러운 광경이군요!

**밀리넵스키:** 그건 그렇지. 근데, 왜 저 소녀는 저 목소리를 가지고 있는거지?

**밀리넵스키의 보좌관:** 어, 뭐라구요?

**밀리넵스키:** 저건.. 아스구 목소리잖아..

**밀리넵스키의 보좌관:** 그러고 보니.. 진짜 닮았군.. 이상하군요..

**밀리넵스키:** 사람들이 나올때까지 기다리세. 저 소녀와 이야기를 나누고 싶네..
