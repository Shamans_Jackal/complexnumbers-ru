---
title: 실현될수 없던 길
subtitle: 밀리넵스키의 아리아
translation: http://transural.egloos.com/314851
translator: GRU Fields
byArtist:
- name: 빅토르 트로피멘코
  role: 아나톨리 밀리넵스키
  image: /images/photos/vit_2.jpg
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**밀리넵스키**

사후의 우리에겐 무엇이 기다리고 있는가?<br>
&emsp;&emsp;&emsp;오직 꿈 없는 느릿한 잠.<br>
오직 수세기의 창백함 뿐인가.<br>
&emsp;&emsp;&emsp;기쁨도, 눈물도 없는?<br>
그 짧은 기간은 어디에 있는가<br>
&emsp;&emsp;&emsp;영원한 세기로 향할 그 기간은<br>
혹여나, 여기, 지금이라지만..<br>
&emsp;&emsp;&emsp;현실 속엔 그런것은 존재할리가 없어...

혹시, 우리가<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;호흡의 중단으로 죽게되면.<br>
&emsp;&emsp;&emsp;꿈에서도 죽게되며<br>
&emsp;&emsp;&emsp;수평선 너머로<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;단순간에 사라지는 것인가?

이 전쟁에서<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;우리는 답을 찾게 될것인가. 이 모든것에 대해..?<br>
&emsp;&emsp;&emsp;실현될수 없던 길을<br>
&emsp;&emsp;&emsp;통과하게 된단 말인가.<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;이 속세라는 곳 속에서..?

무엇때문에 우리는 왜 평화를 중요시 했으며.<br>
&emsp;&emsp;&emsp;우리에게 감정을 느끼게 하는가?<br>
결국 평화는 결코 영원하지 않아<br>
&emsp;&emsp;&emsp;마치 달콤한 꿈같은 것과도 같은 것..<br>
혹여나, 그녀가 옳을지도 -<br>
&emsp;&emsp;&emsp;오직 행복에 젖어 있던건, - 모든걸 생각해뒀기에..?<br>
그렇담 우리 세상이 - 오직 고통 뿐이라면..<br>
&emsp;&emsp;&emsp;우리는 그 세상을 버려주리라!<br>

혹시, 우리는<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;수수께끼들의 답을 해매지만.. 답이 원래 없는 곳에서 찾는 것이 아닐까?<br>
&emsp;&emsp;&emsp;삶이라는 것이 무엇이며,<br>
&emsp;&emsp;&emsp;그것의 가치는 어떠한가에..<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;나에겐 간단한 답변만 주어졌지만.<br>
암흑 속에서 우리는 과연<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;환상빛을 찾아낼수 있을것인가?<br>
&emsp;&emsp;&emsp;실현될수 없던 길을<br>
&emsp;&emsp;&emsp;통과하게 된단 말인가.<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;미래라는 이름의 시대의 안개 속에서..?

그것을 어찌 믿으란 말인가,,!<br>
&emsp;&emsp;&emsp;현세의 나는 널 볼수 없다는 밀을..?<br>
어찌 해야만 한단 말인가..!<br>
&emsp;&emsp;&emsp;다시는 널 볼수가 없다는데...?<br>
그때는 느끼기 힘들었는데.<br>
&emsp;&emsp;&emsp;이상하디 닮은 점을. 진지하게 받아들여야 했지만.<br>
정말 사랑했었어. 마치 꿈과도 같았어.<br>
&emsp;&emsp;&emsp;마치 기적과도 같은. 앞으로 일어날수 있던 기적처럼..

장대비의 빗물이<br>
&emsp;&emsp;&emsp;하나로 뭉쳐져 차가운 장벽을 만들고<br>
열차는 소리없이 날아가네.<br>
&emsp;&emsp;&emsp;검디검은 전자석 위를 타고. 강철의 줄을 형성하고.<br>
이 미친 경쟁 속에서 하나 뿐.<br>
&emsp;&emsp;&emsp;이 밤이 지나가면, 더이상 중요치 않을 것이다.<br>
어째서, 이 세상에 우리를 감싸고 있는지.<br>
&emsp;&emsp;&emsp;어째서, 우리가 언젠가부터 세상에 있었는지..

고통만이 존재하는 세상.<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;삶이라는 권리를 빼앗아가고. 마치 삶을 줬었던 것처럼.<br>
&emsp;&emsp;&emsp;세상은 바꿔져야만 해.<br>
&emsp;&emsp;&emsp;그러지 않으면, 그것은<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;우리의 힘을 인정하지 않겠지.<br>
땅에 떨어진 빗물처럼<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;흘러간 옛 기억처럼. 씻어지리.<br>
&emsp;&emsp;&emsp;실현될수 없던 길.<br>
&emsp;&emsp;&emsp;우린 어쨌든 나아가리.<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;우리 앞에 세상은 굳어버리거라...
</div>
