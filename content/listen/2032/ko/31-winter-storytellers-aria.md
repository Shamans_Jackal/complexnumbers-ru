---
title: 겨울...
translation: http://transural.egloos.com/314857
translator: GRU Fields
byArtist:
- name: 옐레나 야코베츠
  role: 여성 나레이터
  image: /images/photos/len_2.jpg
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**나레이터**

겨울...<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;밤의 어두움 속에.<br>
&emsp;&emsp;&emsp;그을려진 도시 위를 감싸고..<br>
&emsp;&emsp;&emsp;회색 하늘, 마치 하얀 연기와도 같은 하늘 아래에..<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;오지 말아야할 시기에, 그녀는 와버렸어요..<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;그저 우연찮은 결과였을까...<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;아니면.. 운명이라는 것. 일까..<br><br>

그리고.. 세상은..<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;그것는 완전히..<br>
&emsp;&emsp;&emsp;마치 어젯저녓, 아니, 수백년전의 모습으로..<br>
&emsp;&emsp;&emsp;메말라버린 정원만큼 이 광경에게 어울리는 이름이 있을까..?<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;수많은 행성들 중에서..<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;오직 하나의 길이 사라져버렸네요..<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;세기의 암흑 속으로..<br><br>

혹시나, 우린 어딘가 실수한게 아닐까요.<br>
&emsp;&emsp;&emsp;동화와도 같은 꿈에 눈이 먼 경쟁에서..?<br>
혹시나, 우린 너무 늦게 태어난걸지도 몰라요.<br>
&emsp;&emsp;&emsp;정해진 길조차 없는 이 세상 속에서..<br><br>

겨울...<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;끝나버린 경쟁에<br>
&emsp;&emsp;&emsp;어느 누구든 비난할 이를 찾을수 없고,<br>
&emsp;&emsp;&emsp;어느 누구든 하늘에서 새들이 떨어졌는지 물어볼 이 없는,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;오직 죄가 서려있는 시대가<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;운명으로써 모두에게..<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;이렇게..<br><br>

그럼 어서..<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;내리는 잿더미와 눈처럼..<br>
&emsp;&emsp;&emsp;불타버린 시대위에 내려누워 꿈을 꾸자꾸나..<br>
&emsp;&emsp;&emsp;거짓된 섬광이 불어닥치면서, 지금, 승리했다는것에 비웃고..<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;땅에 내리꽃힌 이뤄질수 없는 희망들.<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;밤을 바라보는 탁한 시선들은<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;공허함 뿐..<br><br>

어쩌면, 다시 되돌아 올지도..<br>
&emsp;&emsp;&emsp;어쩌면, 이번때보다 더 행복해 질지도 몰라..<br>
다시금 창공엔 새들이 날아다닐테지..<br>
&emsp;&emsp;&emsp;하지만, 이 세상은 이미 우릴 위한게 아니야...

우린 어딘가에서 수많은 실수를 저질렀어.<br>
&emsp;&emsp;&emsp;동화와도 같은 꿈에 눈이 먼 경쟁에서..<br>
이제 영원히 그곳에 녹아있으리..<br>
&emsp;&emsp;&emsp;흰 화염속, 눈으로 뒤덮힌 지평선 너머로..
</div>