---
title: 인민의 사랑이 담긴 열매들
translation: http://transural.egloos.com/314292
translator: GRU Fields
byArtist:
- name: 데니스 샴린
  role: 나레이터, 밀리넵스키의 비서
  image: /images/photos/den_1.jpg
- name: 빅토르 트로피멘코
  role: 아나톨리 밀리넵스키
  image: /images/photos/vit_2.jpg
- name: 옐레나 야코베츠
  role: 아나운서
  image: /images/photos/len_2.jpg
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---

현대의 카리스마적인 정치가로써<br>
&emsp;&emsp;&emsp;인민을 멀리 둬서는 절대로 아니되며<br>
때로는 인민의 사랑이 담긴 열매들을 받으십시오.<br>
&emsp;&emsp;&emsp;받으시되, 절대로 간단히 여기지 마십시오.

허나 살아있는 대화들을 대체하는건<br>
&emsp;&emsp;&emsp;여행이나 모임을 멀리하면서<br>
TV 속이나, 매체의 힘은 도움이 되지 않습니다.<br>
&emsp;&emsp;&emsp;(허나,) 많은 사람은 자기 자신들을 바꾸기엔 쉽진 않지요..

<br>

**아나운서:** 우리 도시의 제12 번 학교 학동들이 열렬히 다정하신 지도자 밀리넵스키 동지에게 인사 보내며, 제8 번 중학교의 순서로 이어지겠습니다...

<br>

**밀리넵스키:** 저기, 우리 언제 모스크바로 돌아가나?

**밀리넵스키의 보좌관:** 아스구가 저녁에 열차를 보내기로 약속했지만,
정확한 시간은 정해지지 않았습니다. 아시다시피, 여기있는 건 비밀도시잖습니까..

<br>

**아나운서:** 존경하는 아나톨리 세르게예비치,
지금 11학년 A반 여학생인 스베틀라나 리마예바가 무대에 나오고 있습니다.
스베틀라나는 우리의 위대한 당과 정부, 특히 서기장 동지를 위해 노래를 헌정한다 합니다.
