---
title: 에필로그
translation: http://transural.egloos.com/314854
translator: GRU Fields
byArtist:
- name: 데니스 샴린
  role: 나레이터
  image: /images/photos/den_1.jpg
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---

<div class="poem">

**나레이터**

...현재 우리는 압니다 : 이 세상에<br>
&emsp;&emsp;&emsp;운명론을 위한 자리는 없다고 말이지요..<br>
그의 법칙은 넓게 퍼져있었고.<br>
&emsp;&emsp;&emsp;행성의 운동법칙 보다 복잡하지요..

다만 우리가 "확률"을 알수 있게 된다면<br>
&emsp;&emsp;&emsp;만약에 그렇다면, - 나의 귀인이여.<br>
거기서 나오는 모든 경우의 수들은<br>
&emsp;&emsp;&emsp;우리에게 하나의 답만을.. 도출할 뿐이지요..

</div>