---
title: 애매하디 다가가는 결실
subtitle: 밀리넵스키의 아리아
translation: http://transural.egloos.com/314281
translator: GRU Fields
byArtist:
- name: 빅토르 트로피멘코
  role: 아나톨리 밀리넵스키
  image: /images/photos/vit_2.jpg
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**밀리넵스키**

&emsp;&emsp;&emsp;아직 4년밖에 지나지 않았건만.<br>
주위를 둘러보면  얼마나 변해가는가.<br>
아름드리 초지가 얼마나 빠르게 건네오는가.<br>
&emsp;&emsp;&emsp;애매하디 애매하게 찾아오는 열매여.

&emsp;&emsp;&emsp;그대, 그저 무심하듯 있구나.<br>
어느 질문이에도 언제나 더 명백해지고<br>
지구상의 세상도 명백히 아름다워지리.<br>
&emsp;&emsp;&emsp;출기의 때에도 별 신경을 쓰지 않지

&emsp;&emsp;&emsp;그리고 너는 알지 못하지 않느냐<br>
의혹과 주저함, 부끄러움이 무엇인지도<br>
그리고 원한이 무엇인지도..<br>
&emsp;&emsp;&emsp;이전의 선택에 후회라는 단어도.

&emsp;&emsp;&emsp;우리가 무엇을 목숨지켜야 할 가치가 있는지도.<br>
어찌보면 이미 깨달았을지도 모르지.<br>
너무나도 빠르게 앞으로 나아가려 하는 것과<br>
&emsp;&emsp;&emsp;몆 사람들이 너를 믿지 않고 있다는 것도?

자동화 - 그것은 자연스런 과정.<br>
&emsp;&emsp;&emsp;전 지구의 모두에 포함되는 말.<br>
또한 수세기동안의 투쟁도 헛 것이 되는 것.<br>
&emsp;&emsp;&emsp;모든 혁명과, 그 위대한 승리들은 어찌 되는 것인가?

하지만, 그 목표는 공산주의의 무한기관을 이루게 될지.<br>
&emsp;&emsp;&emsp;혹여, 우리의 생산 노동가치는 박탈되버리고<br>
우리의 협동 - 이어져오던 모든 일념도<br>
&emsp;&emsp;&emsp;결국 생산과정의 미숙했던 모델이였단 말인가..?!
</div>
