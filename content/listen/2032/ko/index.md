---
title: "2032: 미실현미래전설"
artist: Victor Argonov Project
playlistTitle: "2032<br>미실현미래전설"
image: /listen/2032/cover-clean.jpg
overlay: /listen/2032/overlay-ko.svg

formatters:
  albumPageTitle: "%album_name% — %artist%"
  trackPageTitle: "%track_title% — %album_name% — %artist%"
  trackTitle: "%track_title% (%track_subtitle%)"
  infoTitle: "%album_name%"

meta:
  description: "2032: The Legend of the Lost Future is a techno-opera created as part of Victor Argonov Project. It tells the story of the Soviet Union in 2032, touching on the problems of artificial intelligence, industrial automation, Marxist ideology, the balance between utopia and practice, the ratio of technical progress, human values, politics and ideology."
---

**2032: Legend of a Lost Future** is the first hour-and-a-half long electronic opera by Vladivostok composer Victor Argonov.

> This is neither anti-utopia, nor utopia, nor grotesque. Rather, an attempt at a fantastic rethinking of the communist idea, its painful romanticism and tragedy, the clash of the unattainable Christian ideal of universal love and rationalist economic goals. Individual love and universal love, local war and global war, the search for paradise on earth and the desire for self-destruction, artificial reason and human reason... There is no propaganda for or against, there is only a problem. And, like the fantasy genre, where the medieval setting does not mean the author's desire to return to the Middle Ages, here socialism also does not claim to be political pathos - it only creates an atmosphere, a certain aesthetic of what is happening.

2032: Legend of a Lost Future is considered to be the first electronic opera with compositions available as source code under the [CC-BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/) license on the page [https://complexnumbers.ru/2032/](https://complexnumbers.ru/2032/).
