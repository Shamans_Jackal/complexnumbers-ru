---
title: 프롤로그
translation: http://transural.egloos.com/314199
translator: GRU Fields
byArtist:
- name: 데니스 샴린
  role: 나레이터
  image: /images/photos/den_1.jpg
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---

<div class="poem">

**나레이터**

현재 우리는 압니다 : 이 세상에<br>
&emsp;&emsp;&emsp;운명론을 위한 자리는 없다고 말이지요..<br>
그의 법칙은 넓게 퍼져있었고.<br>
&emsp;&emsp;&emsp;행성의 운동법칙 보다 복잡하지요..

다만 우리가 "확률"을 알수 있게 된다면<br>
&emsp;&emsp;&emsp;만약에 그렇다면, - 나의 귀인이여.<br>
거기서 나오는 모든 경우의 수 들과<br>
&emsp;&emsp;&emsp;우리에게 하나의 답을 비추어주고

무궁무진한 가능성이 함께할 것이며<br>
&emsp;&emsp;&emsp;오직 하나의 진정한 길을 인도할겁니다.<br>
그리고 그것이 실현된다면,<br>
&emsp;&emsp;&emsp;무너지지도, 지워지지 않고

다시 되감을수 있다면..<br>
&emsp;&emsp;&emsp;정말로 지나가 버렸던, 그 시절.<br>
앞으로 일어날 일을 예상했습니다.<br>
&emsp;&emsp;&emsp;그때 우리가 할수 없었던 일을...!

</div>
