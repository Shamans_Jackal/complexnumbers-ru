---
title: 'Меж двух эпох — Complex Numbers'
image: /images/covers/btta.jpg

album:
  title: Меж двух эпох (2016–2022)
  description: 11 треков разных лет, в том числе со страницами обсуждений.
  artist: Complex Numbers
  cover: /images/covers/btta.jpg
  credits: |
    Музыка и тексты: Виктор Аргонов. Вокал: Len, Oleviia, Ariel, Елена Яковец.

  mp3: https://gitlab.com/api/v4/projects/32378456/packages/generic/release/1.0.0/2022-between-the-two-ages-mp3.zip
  vk: https://vk.com/music/playlist/-23865151_72220826_650ebc74bd8bbb3fa0
  gdrive: https://drive.google.com/drive/folders/1ImlLGUwP_Ig9fZi8ISDVYK8A2m2pS6Bz

  tracks:
  - title: Время придёт [Версия 2016] (Изучает прогресс поведение людей)
    description: Музыка - Виктор Аргонов, Владимир Матецкий, текст - Виктор Аргонов, Арсений Тарковский, вокал - Len, Ariel
    url: https://complexnumbers.gitlab.io/releases/between-the-two-ages/mp3/01-time-will-come-2016-version.mp3
    links:
    - text: История и обсуждение
      url: https://argonov.livejournal.com/204039.html

  - title: Пролегла черта (Меж двух эпох)
    description: Музыка, текст - Виктор Аргонов, вокал - Oleviia
    url: https://complexnumbers.gitlab.io/releases/between-the-two-ages/mp3/02-the-line-has-been-drawn.mp3

  - title: Прото-увертюра 2032
    description: Музыка - Илья Пятов, Виктор Аргонов
    url: https://complexnumbers.gitlab.io/releases/between-the-two-ages/mp3/03-proto-overture-2032.mp3

  - title: Межкарантинное лето [С цитатами МакSим и Тухманова]
    description: Музыка - Виктор Аргонов, МакSим, Давид Тухаманов, текст - Виктор Аргонов, МакSим, вокал - Len
    url: https://complexnumbers.gitlab.io/releases/between-the-two-ages/mp3/04-summer-between-quarantines.mp3
    links:
    - text: Разбор смысла
      url: https://vk.com/@viktorargonovproject-2011-11-18

  - title: В дни чудес (Три бозона)
    description: Музыка, текст - Виктор Аргонов, вокал - Oleviia
    url: https://complexnumbers.gitlab.io/releases/between-the-two-ages/mp3/05-in-the-days-of-wonder.mp3
    links:
    - text: Разбор смысла
      url: https://vk.com/@viktorargonovproject-complex-numbers-v-dni-chudes-tri-bozona-smysl-pesni

  - title: 200 минут [Версия 2020] (Плен инстинктов жесток)
    description: Музыка, текст - Виктор Аргонов, вокал - Елена Яковец, Len
    url: https://complexnumbers.gitlab.io/releases/between-the-two-ages/mp3/06-200-minutes-2020-version.mp3

  - title: Сердцу прикажешь (К чувствам ключи)
    description: Музыка, текст - Виктор Аргонов, вокал - Len
    url: https://complexnumbers.gitlab.io/releases/between-the-two-ages/mp3/07-the-heart-will-obey-you.mp3
    links:
    - text: История и обсуждение
      url: https://argonov.livejournal.com/227383.html

  - title: Пробуждение [Версия 2020] (Никто не решал, кем родиться)
    description: Музыка, текст - Виктор Аргонов, вокал - Len
    url: https://complexnumbers.gitlab.io/releases/between-the-two-ages/mp3/08-awakening-2020-version.mp3

  - title: Воздух снов [Инструментальная версия 2022]
    description: Музыка - Виктор Аргонов
    url: https://complexnumbers.gitlab.io/releases/between-the-two-ages/mp3/09-air-of-dreams-2022-instrumental-version.mp3

  - title: 1000 лет назад (Мне звёзды казались так близко)
    description: Музыка, текст - Виктор Аргонов, вокал - Len
    url: https://complexnumbers.gitlab.io/releases/between-the-two-ages/mp3/10-1000-yars-ago.mp3
    links:
    - text: История и обсуждение
      url: https://argonov.livejournal.com/227741.html

  - title: Время придёт [Версия 2022] (Изучает прогресс поведение людей)
    description: Музыка - Виктор Аргонов, Владимир Матецкий, текст - Виктор Аргонов, Арсений Тарковский, вокал - Len, Ariel
    url: https://complexnumbers.gitlab.io/releases/between-the-two-ages/mp3/11-time-will-come-2022-version.mp3

---
